var RIDES = {
    initialize: function () {
        RIDES.loadUserInfo();
        RIDES.editFromLocalStorage();
    },
    initializeEvents: function () {
          
        if (document.getElementById('save')) {
            
            document.getElementById('save').addEventListener('click', function () {
                
                var rides = [];
                    
                    dataInLocalStorage = localStorage.getItem('rides');
                    
                    rides = JSON.parse(dataInLocalStorage);

                    var index = localStorage.getItem('position');
                    
                   // 
                    rides.splice(index, 1);

                    localStorage.setItem('rides', JSON.stringify(rides));
                    add();
                    
            

    });
    function add(){
        var dayscheck = [];

        for (i = 0; i < 7; i++) {
            var array = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"];
           
            if (document.getElementById(array[i]).checked) { 
                dayscheck.push(document.getElementById(i).innerHTML);
                
                console.log(document.getElementById(i).innerHTML);
                
            }
        };

        var ride = {
            userName: RIDES.loadUserInfo(),
            name: document.getElementById('ridename').value,
            start: document.getElementById('autocomplete').value,
            end: document.getElementById('autocomplete2').value,
            description: document.getElementById('description').value,
            departure: document.getElementById('departure').value,
            arrival: document.getElementById('arrival').value,
            days: dayscheck
        };
        RIDES.addRide(ride);
        location.href = "dashboard.html";
         
    }

    }if (document.getElementById('logout')) {
        document.getElementById('logout').addEventListener('click', function () {
            RIDES.logout();
        });
        
    }
    },
    editFromLocalStorage: function (){

        var ride =[];
        ride= JSON.parse( localStorage.getItem('editar'));
        
  
        //localStorage.setItem('position', index);

        
        document.getElementById("ridename").value = ride.name;   
        document.getElementById("autocomplete").value = ride.start;
        document.getElementById("autocomplete2").value = ride.end;
        document.getElementById("description").value = ride.description;
        document.getElementById("departure").value = ride.departure;
        document.getElementById("arrival").value = ride.arrival;

     

        for (i = 0; i < ride.days.length; i++) {

            document.getElementById(ride.days[i].toLowerCase()).checked = true;
            
        }

        // rides.splice(index, 1);

        // localStorage.setItem('rides', JSON.stringify(rides));

        // RIDES.loadUserRides();
    },
    addRide: function (ride) {
      
    var rides = [];
    if (localStorage.getItem('rides')) {
        rides = JSON.parse(localStorage.getItem('rides'));
    }
    rides.push(ride);
    localStorage.setItem('rides', JSON.stringify(rides));
    
 

},
    loadUserInfo: function () {
    
    if (localStorage.getItem('islogin')) {
        var user = [];
        user = JSON.parse(localStorage.getItem('islogin'));
        if (document.getElementById('settings')) {
            document.getElementById('full_name').value = user.firstName + " " + user.lastName;
            document.getElementById('speed').value = user.speed;
            document.getElementById('aboutme').value = user.aboutme;
        }

        if (document.getElementById('userName')) {
            var username = document.getElementById('userName');
            
            username.innerHTML = user.userName + username.innerHTML;
        }
        return user.userName;
    }
    },

    
    logout: function () {
        if (localStorage.getItem('islogin')) {
            localStorage.removeItem('islogin');
            location.href = "index.html";
        }
    } 


};
RIDES.initialize();
RIDES.initializeEvents();
