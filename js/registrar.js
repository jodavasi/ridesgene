var RIDES = {
   

    initializeEvents: function () {
      
    var formu = document.getElementsByName('formulario')[0],
    elementos = formu.elements,
    boton = document.getElementById('btn');

        if (boton) { 
            formu.addEventListener('submit', function () {
               
            

                    // obtener la información del form
                    var user = {
                        userName: formu.username.value,
                        password: formu.pass.value,
                        firstName: formu.firstname.value,
                        lastName: formu.lastname.value,
                        phone: formu.phone.value,
                        speed: "60",
                        aboutme: ""
                    };


                    if (user.userName !== "" && user.firstName.length > 1 && user.lastName.length > 1 && user.password.length > 2) {

                        if (RIDES.addUser(user) === 0) {
                            alert('Successful registration');
                            setTimeout(function(){
                                location.reload("./registrar.html"); 
                            },1);

                        } else {
                            
                            alert('User Already Exist!');
                            setTimeout(function(){
                                location.reload("./registrar.html"); 
                            },1);
                        }
                    }

            });
        }



 

},
addUser: function (user) {
    // agregarlo a localStorage
    var users = [],
        cant = 0;

    if (localStorage.getItem('users')) {
        users = JSON.parse(localStorage.getItem('users'));
    }
  


    users.forEach(function (user1) {
        if (user1.userName === user.userName) {
            cant++;
        }
    });

    if (cant === 0) {
        users.push(user);
        localStorage.setItem('users', JSON.stringify(users));
    }
    return cant;
}

};


RIDES.initializeEvents();