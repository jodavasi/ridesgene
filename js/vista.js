var RIDES = {
    initialize: function () {
        RIDES.loadUserInfo();
        RIDES.vista();
    },
    
    vista: function (){

        var ride =[];
        ride= JSON.parse( localStorage.getItem('vista'));
        
  
        //localStorage.setItem('position', index);

        
        document.getElementById("ridename").value = ride.name;   
        document.getElementById("autocomplete").value = ride.start;
        document.getElementById("autocomplete2").value = ride.end;
        document.getElementById("description").value = ride.description;
        document.getElementById("departure").value = ride.departure;
        document.getElementById("arrival").value = ride.arrival;

     

        for (i = 0; i < ride.days.length; i++) {

            document.getElementById(ride.days[i].toLowerCase()).checked = true;
            
        }

        // rides.splice(index, 1);

        // localStorage.setItem('rides', JSON.stringify(rides));

        // RIDES.loadUserRides();
    },
    
    loadUserInfo: function () {
    
    if (localStorage.getItem('islogin')) {
        var user = [];
        user = JSON.parse(localStorage.getItem('islogin'));
        if (document.getElementById('settings')) {
            document.getElementById('full_name').value = user.firstName + " " + user.lastName;
            document.getElementById('speed').value = user.speed;
            document.getElementById('aboutme').value = user.aboutme;
        }

        if (document.getElementById('userName')) {
            var username = document.getElementById('userName');
            
            username.innerHTML = user.userName + username.innerHTML;
        }
        return user.userName;
    }
    },

    
    logout: function () {
        if (localStorage.getItem('islogin')) {
            localStorage.removeItem('islogin');
            location.href = "index.html";
        }
    } 


};
RIDES.initialize();

