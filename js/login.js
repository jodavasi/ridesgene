//document.getElementById("login").addEventListener("click", validar);

var RIDES = {
    initializeEvents: function () {
       
if (document.getElementById('login')) {
    document.getElementById('login').addEventListener('click', function () {
        // obtener la información del form
        RIDES.Login();
    });
}
    },
    Login: function () {
    if (document.getElementById('username') && document.getElementById('pass')) {
        
        var users = [];
        if (localStorage.getItem('users')) {
            users = JSON.parse(localStorage.getItem('users'));

            users.forEach(function (user, index, users) {
                if (user.userName === document.getElementById('username').value && 
                user.password === document.getElementById('pass').value) {
                   
                    localStorage.setItem('islogin', JSON.stringify(user));
                    setTimeout(function(){
                        location.href = "dashboard.html";  
                    },1);
                }
            });
        }
    }
    }
};
RIDES.initializeEvents();