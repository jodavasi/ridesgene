var RIDES = {
    initialize: function () { 
        
        if (document.getElementById('myTableData1')) {
            RIDES.loadUserRides();
        }
       RIDES.loadUserInfo();
    },
    

    loadUserRides: function () {
        if (localStorage.getItem('islogin')) {
            if (document.getElementById('myTableData1')) {
                var rides = [];

                RIDES.clearTable();

                if (localStorage.getItem('rides')) {
                    rides = JSON.parse(localStorage.getItem('rides'));
                }

                var table = document.getElementById("myTableData1");
                var user = JSON.parse(localStorage.getItem('islogin'));
             
                rides.forEach(function (ride, index) {
                    if (ride.userName === user.userName) {
                       
                       
                        var tr = document.createElement("tr"),
                        tdName = document.createElement("td"),
                        tdJob = document.createElement("td"),
                        tdAge = document.createElement("td"),
                        tdRemove = document.createElement("td"),
                        btnEdit = document.createElement("button");
                        btnRemove = document.createElement("button");

                    tdName.innerHTML = ride.name;
                    tdJob.innerHTML = ride.start;
                    tdAge.innerHTML = ride.end;

                    btnRemove.textContent = 'Delete';
                    btnRemove.id='bot';
                    btnRemove.className = 'btn btn-xs btn-danger';
                    btnRemove.addEventListener('click', function(){
                        // alert(rides);
                        // alert(index);
                        removeFromLocalStorage(index);
                    });

                    btnEdit.textContent = 'Edit';
                    btnEdit.id='ed';
                    btnEdit.className = 'btnn btn-xs btn-danger';
                    btnEdit.addEventListener('click', function(){
                        editFromLocalStorage(index);
                        location.href = "edit.html";
                    });
                    tdRemove.appendChild(btnEdit);
                    tdRemove.appendChild(btnRemove);

                    tr.appendChild(tdName);
                    tr.appendChild(tdJob);
                    tr.appendChild(tdAge);
                    tr.appendChild(tdRemove);

                    table.appendChild(tr);
                    }
                });
                 function removeFromLocalStorage(index){
                     
                    var rides = [];
                    
                    dataInLocalStorage = localStorage.getItem('rides');
                    
                    rides = JSON.parse(dataInLocalStorage);
            
                    rides.splice(index, 1);
            
                    localStorage.setItem('rides', JSON.stringify(rides));
            
                    RIDES.loadUserRides();
                }

                function editFromLocalStorage(index){
                    
                    var rides = [];
                    
                    dataInLocalStorage = localStorage.getItem('rides');
                    
                    rides = JSON.parse(dataInLocalStorage);
                    
                    localStorage.setItem('position', index);

                    
                    rides.forEach(function (ride, index2) {
                        
                      if(index2===index){
                          
                          localStorage.setItem('editar', JSON.stringify(ride));
                          
                    }
                    });

                    // rides.splice(index, 1);
            
                    // localStorage.setItem('rides', JSON.stringify(rides));
            
                    // RIDES.loadUserRides();
                }
            
            }
        }if (document.getElementById('logout')) {
            document.getElementById('logout').addEventListener('click', function () {
                RIDES.logout();
            });
            
        }
    },
    loadUserInfo: function () {
        
        if (localStorage.getItem('islogin')) {
            var user = [];
            user = JSON.parse(localStorage.getItem('islogin'));
            if (document.getElementById('settings')) {
                document.getElementById('full_name').value = user.firstName + " " + user.lastName;
                document.getElementById('speed').value = user.speed;
                document.getElementById('aboutme').value = user.aboutme;
            }

            if (document.getElementById('userName')) {
                var username = document.getElementById('userName');
                var usernameside = document.getElementById('userNameSide');
                username.innerHTML = user.userName + username.innerHTML;
                usernameside.innerHTML = usernameside.innerHTML + user.userName;
            }

            return user.userName;
        }
    },
    logout: function () {
        if (localStorage.getItem('islogin')) {
            localStorage.removeItem('islogin');
            location.href = "index.html";
        }
    },
    clearTable: function (table) {
        if (document.getElementById('myTableData')) {
            document.getElementById("myTableData").innerHTML = "";
        }
        if (document.getElementById('myTableData1')) {
            document.getElementById("myTableData1").innerHTML = "";
        }

    }

};
RIDES.initialize();
