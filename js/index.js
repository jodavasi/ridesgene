var RIDES = {

    initializeEvents: function () {

        if (document.getElementById('buscar')) {
            document.getElementById('buscar').addEventListener('click', function () {
                // obtener la información del form
                var place = {
                    start: document.getElementById('autocomplete').value,
                    end: document.getElementById('autocomplete2').value
                };
                RIDES.loadRides(place);
            });
        }

    },
    loadRides: function (place) {
        if (document.getElementById('myTableData')) {
            RIDES.clearTable();
            var rides = [];

            if (localStorage.getItem('rides')) {
                rides = JSON.parse(localStorage.getItem('rides'));
            }


            rides.forEach(function (ride, index, rides) {
                var table = document.getElementById("myTableData");
                if ((place.start === "" || place.end === "") || (ride.start === place.start && ride.end === place.end)) {
                    
                    var tr = document.createElement("tr"),
                    tdName = document.createElement("td"),
                    tdJob = document.createElement("td"),
                    tdAge = document.createElement("td"),
                    tdRemove = document.createElement("td"),
                    btnRemove = document.createElement("button");

                tdName.innerHTML = ride.name;
                tdJob.innerHTML = ride.start;
                tdAge.innerHTML = ride.end;

                btnRemove.textContent = 'VIEW';
                btnRemove.id='vista';
                btnRemove.className = 'btn btn-xs btn-danger';
                btnRemove.addEventListener('click', function(){
                    // alert(rides);
                    // alert(index);
                    vista(index);
                    location.href = "view_log.html";
                
                });
                tdRemove.appendChild(btnRemove);

                    tr.appendChild(tdName);
                    tr.appendChild(tdJob);
                    tr.appendChild(tdAge);
                    tr.appendChild(tdRemove);

                    table.appendChild(tr);
                    
                }
            });
            function vista(index){
                    
                var rides = [];
                
                dataInLocalStorage = localStorage.getItem('rides');
                
                rides = JSON.parse(dataInLocalStorage);
                
                localStorage.setItem('position', index);

                
                rides.forEach(function (ride, index2) {
                    
                  if(index2===index){
                      
                      localStorage.setItem('vista', JSON.stringify(ride));
                      
                }
                });

                // rides.splice(index, 1);
        
                // localStorage.setItem('rides', JSON.stringify(rides));
        
                // RIDES.loadUserRides();
            }
         
        }

    
    },

    addModals: function (num, ride) {

        var ni = document.getElementById('myDiv');

        var newdiv = document.createElement('div');

        var divIdName = 'modal' + num;

        var days = ride.days;

        newdiv.setAttribute('id', divIdName);
        newdiv.setAttribute('class', 'modal');

        var daysHtml = '';
        days.forEach(function (day, index, days) {
            daysHtml += '<li>' + day + '</li>';
        });

        newdiv.innerHTML = '<div class="modal-content"><ul><li>Start from: <span>' + ride.start + '</span></li><li>End: <span>' + ride.end + '</span></li><li>Description: <span>' + ride.description + '</span></li><br><li>Departure: <span>' + ride.departure + '</span></li><li>Estimated Arrival: <span>' + ride.arrival + '</span></li><br><br><ul>Days' + daysHtml + '</ul></ul></div><div class="modal-footer"><a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a></div>';

        ni.appendChild(newdiv);
    },
    //terminado
    clearTable: function (table) {
        if (document.getElementById('myTableData')) {
            document.getElementById("myTableData").innerHTML = "";
        }
        if (document.getElementById('myTableData1')) {
            document.getElementById("myTableData1").innerHTML = "";
        }

    }

};RIDES.initializeEvents();