var RIDES = {

    initialize: function () { 
        
       RIDES.loadUserInfo();
    },

    initializeEvents: function () {
      
        
        if (document.getElementById('addRide')) {
            
            document.getElementById('addRide').addEventListener('click', function () {
                

                    var dayscheck = [];

                    for (i = 0; i < 7; i++) {
                        var array = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"];
                        
                        if (document.getElementById(array[i]).checked) {
                            dayscheck.push(document.getElementById(i).innerHTML);
                            
                            console.log(document.getElementById(i).innerHTML);
                            
                        }
                    };

                    var ride = {
                        userName: RIDES.loadUserInfo(),
                        name: document.getElementById('ridename').value,
                        start: document.getElementById('autocomplete').value,
                        end: document.getElementById('autocomplete2').value,
                        description: document.getElementById('description').value,
                        departure: document.getElementById('departure').value,
                        arrival: document.getElementById('arrival').value,
                        days: dayscheck
                    };
                     
                    RIDES.addRide(ride);
                    location.href = "dashboard.html";
                
            });
        }
        if (document.getElementById('logout')) {
            document.getElementById('logout').addEventListener('click', function () {
                RIDES.logout();
            });
            
        }
    },
    loadUserInfo: function () {
    
    if (localStorage.getItem('islogin')) {
        var user = [];
        user = JSON.parse(localStorage.getItem('islogin'));
        if (document.getElementById('settings')) {
            document.getElementById('full_name').value = user.firstName + " " + user.lastName;
            document.getElementById('speed').value = user.speed;
            document.getElementById('aboutme').value = user.aboutme;
        }

        if (document.getElementById('userName')) {
            var username = document.getElementById('userName');
            
            username.innerHTML = user.userName + username.innerHTML;
        }
        return user.userName;
    }
    },
    logout: function () {
        if (localStorage.getItem('islogin')) {
            localStorage.removeItem('islogin');
            location.href = "index.html";
        }
    },

    addRide: function (ride) {
       
    var rides = [];
    if (localStorage.getItem('rides')) {
        rides = JSON.parse(localStorage.getItem('rides'));
    }
    rides.push(ride);
    localStorage.setItem('rides', JSON.stringify(rides));
    }


   

};
RIDES.initialize();
RIDES.initializeEvents();