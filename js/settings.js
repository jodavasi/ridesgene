var RIDES = {
    initialize: function () {
        RIDES.loadUserInfo();
    },
    initializeEvents: function () {
        if (document.getElementById('logout')) {
            document.getElementById('logout').addEventListener('click', function () {
                RIDES.logout();
            });
            
        }
        if (document.getElementById('editUser')) {
            document.getElementById('editUser').addEventListener('click', function () {

                    // obtener la información del form
                    var userInfo = {
                        speed: document.getElementById('speed').value,
                        aboutme: document.getElementById('aboutme').value
                    };

                    if (userInfo.speed > 0) {

                        RIDES.editUser(userInfo);

                            location.href = "dashboard.html";
                       
                    }
                
            })
        }



    },
    loadUserInfo: function () {
    
        if (localStorage.getItem('islogin')) {
            var user = [];
            user = JSON.parse(localStorage.getItem('islogin'));
            if (document.getElementById('settings')) {
                document.getElementById('fullname').value = user.firstName + " " + user.lastName;
                document.getElementById('speed').value = user.speed;
                document.getElementById('aboutme').value = user.aboutme;
            }
    
            if (document.getElementById('userName')) {
                var username = document.getElementById('userName');
                
                username.innerHTML = user.userName + username.innerHTML;
            }
            return user.userName;
        }
    },
    
    editUser: function (userInfo) {
        var users = [],
            usersBackup = [],
            logon = [];

        if (localStorage.getItem('users')) {
            usersBackup = JSON.parse(localStorage.getItem('users'));
        }
        if (localStorage.getItem('islogin')) {
            logon = JSON.parse(localStorage.getItem('islogin'));
        }
        usersBackup.forEach(function (user, index, usersBackup) {
            if (user.userName === logon.userName) {
                user.speed = userInfo.speed;
                user.aboutme = userInfo.aboutme;
                logon = user;
            }
            users.push(user);
        });
        localStorage.setItem('users', JSON.stringify(users));
        localStorage.setItem('islogin', JSON.stringify(logon));
        location.href = "dashboard.html";
    },
        
    logout: function () {
            if (localStorage.getItem('islogin')) {
                localStorage.removeItem('islogin');
                location.href = "index.html";
            }
    } 
    
    


};
RIDES.initialize();
RIDES.initializeEvents();